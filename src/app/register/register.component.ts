import { Component, OnInit, Injectable  } from '@angular/core';
import { User } from '../model/user';
import { Router } from '@angular/router';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  userModal = new User();

  constructor(
    private router: Router
  ) { }

  ngOnInit() {
  }

  onSubmit(formData){
    if(formData.invalid){
      return;
    } 
    localStorage.setItem("userInfo", JSON.stringify(this.userModal));
    console.log(localStorage.getItem("userInfo"));
    this.router.navigate(['']);
  }
}
