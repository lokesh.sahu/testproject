export class User {
    public id: number;
    public name: string;
    public password: string;
    public cource: string;
    public city: string;
    public state: string;
    public country: string;
}