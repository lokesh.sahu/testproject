import { Component, OnInit } from '@angular/core';
import { User } from '../model/user';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  userModal = new User();
  pleaseReg = 'Please register';
  regStatus= true;
  invalidCedStatus = false;
  InvalidMsg="Invalid credentials";
  constructor(
    private router: Router
  ) { }

  ngOnInit() {
    
  }

  onSubmit(formData){
    if(formData.invalid){
      return;
    } 
    if(!localStorage.getItem("userInfo")){
      this.regStatus = false;
      return false;
    }
    let userData = JSON.parse(localStorage.getItem("userInfo"));
    console.log(userData.id);
    if(userData.id != this.userModal.id || userData.password !=this.userModal.password){
      this.invalidCedStatus = true;
      return;
    }
    this.router.navigate(['dashboard']);
  }

}
